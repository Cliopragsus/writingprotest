﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Text introductoryText;
    public string[] introductoryTexts;
    public string[] storyTextsBad;
    public string[] storyTextsNeutral;

    public int index = 0;
    public int indexIntroductory = 0;

    private int[] storyPoints = new int[] { 3, 6, 9, 12, 15, 18, 21 };
    private bool makingChoices = true;

    [Space(10)]
    [Header ("Choices")]
    public Button[] choiceOne;
    public string[] choiceOneTexts;
    public Button[] choiceTwo;
    public string[] choiceTwoTexts;
    public Button[] choiceThree;
    public string[] choiceThreeTexts;

    [Space(10)]
    private int suspicion;
    public Text suspicionText;
    public int maxSuspicion;

    public Text transitionalText;

    private bool checkedForStory;

    private void Start()
    {
        UpdateTexts();
        index = 0;
    }

    public void Continue()
    {
        if (makingChoices)
        {
            indexIntroductory++;
            index += 3;
            UpdateTexts();
        }
        SetButtons(true);

        if (!checkedForStory)
        {
            checkedForStory = true;
            foreach (int point in storyPoints)
            {
                if (point == index)
                {
                    makingChoices = false;
                    SetButtons(false);
                    if (suspicion > maxSuspicion)
                    {
                        introductoryText.text = storyTextsBad[(point / 3) -1];
                    }
                    else
                    {
                        introductoryText.text = storyTextsNeutral[(point / 3) -1];
                    }
                }
            }
        }
        else
        {
            checkedForStory = false;
            makingChoices = true;
            UpdateTexts();
        }
    }

    private void SetButtons(bool state)
    {
        foreach (Button button in choiceOne)
        {
            button.interactable = state;
        }
        foreach (Button button in choiceTwo)
        {
            button.interactable = state;
        }
        foreach (Button button in choiceThree)
        {
            button.interactable = state;
        }
    }

    public void UpdateTexts()
    {
        introductoryText.text = introductoryTexts[indexIntroductory];

        foreach (Button choice in choiceOne)
        {
            choice.GetComponentInChildren<Text>().text = choiceOneTexts[index];
            index++;
        }
        index -= choiceOne.Length;
        foreach (Button choice in choiceTwo)
        {
            choice.GetComponentInChildren<Text>().text = choiceTwoTexts[index];
            index++;
        }
        index -= choiceTwo.Length;
        foreach (Button choice in choiceThree)
        {
            choice.GetComponentInChildren<Text>().text = choiceThreeTexts[index];
            index++;
        }
        index -= choiceThree.Length;
    }

    private void UpdateSuspicionText()
    {
        suspicionText.text = "Suspicion : " + suspicion + " / " + maxSuspicion;
    }

    public void B_choiceOne(int line)
    {
        suspicion += 1;
        UpdateSuspicionText();
        choiceTwo[line].interactable = false;
        choiceThree[line].interactable = false;
    }

    public void B_choiceTwo(int line)
    {
        choiceOne[line].interactable = false;
        choiceThree[line].interactable = false;
    }

    public void B_choiceThree(int line)
    {
        suspicion -= 1;
        UpdateSuspicionText();
        choiceOne[line].interactable = false;
        choiceTwo[line].interactable = false;
    }
}
